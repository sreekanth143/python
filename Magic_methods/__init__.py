# __init__: The __init__ method is used to initialize an object after it has been created.
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

person = Person('John', 30)
print(person.name) # Output: John
print(person.age) # Output: 30

















