Magic methods - 
__add__ = defines the behaviour of the + operator for an object.

__call__ = to make an object callable like a function.

__contains__ = used to check if an object is a member of a container object.

__init__ = used to initialize an object after it has been created.

__new__ = used to create a new instance of the class.

__slots__ = used to limit the attributes of the class.

__str__ = used to provide the string representation of an object.