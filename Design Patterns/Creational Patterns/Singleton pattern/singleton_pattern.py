# Singleton is a creational design pattern that lets you ensure that a class has only one instance, while providing a global access point to this instance.
# Use the Singleton pattern when a class in your program should have just a single instance available to all clients; for example, a single database object shared by different parts of the program.
# Use the Singleton pattern when you need stricter control over global variables.
# Usage examples: A lot of developers consider the Singleton pattern an antipattern. That’s why its usage is on the decline in Python code.

# Identification: Singleton can be recognized by a static creation method, which returns the same cached object.
